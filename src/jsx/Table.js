import React, { Component } from 'react'
import Swal from 'sweetalert2'
import "../css/style.css"

export class Table extends Component {
    constructor(){
        super();
    }

//button alert message 
    onClickBtn2 =(data)=>{
        var email = data.email === ""?"null": data.email;
        var name = data.username === ""?"null": data.username;
        var age = data.age === ""?"null": data.age;
        // console.log(email);
        Swal.fire({ 
            title : 'ID: ' + data.id + '\nEmail: '+ email + '\nUsername: '+ name + '\nAge: '+ age
        });            
    }

  render() {
    return (
      <div className="w-5/6">
        <table className="table-full w-full m-auto text-xl my-20 text-center">
            <thead >
                <tr className="">
                <th className="p-3 px-10 w-32 font-semibold ">ID</th>
                <th className=" p-3 uppercase w-60 font-semibold">Email</th>
                <th className=" p-3 uppercase w-60 font-semibold">Username</th>
                <th className=" p-3 uppercase w-48 font-semibold">Age</th>
                <th className=" p-3 uppercase font-semibold">Action</th>
                </tr>
            </thead>
            <tbody>
                {this.props.data.map((item)=>(
                    <tr key={item.id} className="text-xl">
                        <td className=" p-3">{item.id}</td>
                        <td className=" p-3">{item.email === ""?"null" : item.email}</td>
                        <td className=" p-3">{item.username  === ""?"null" : item.username}</td>
                        <td className=" p-3">{item.age === ""?"null" : item.age}</td>
                        <td className=" p-3">
                            <td className="pl-3">
                                <button onClick={()=>this.props.fun(item.id)} id="btn-1" className={item.status === "Pending"?" text-white w-60 py-2 rounded-md bg-red-700 hover:bg-red-600" : " text-white w-60 py-2 rounded-md bg-green-800 hover:bg-green-600"}> {item.status}</button>
                            </td>
                            <td>
                                <button onClick={()=>this.onClickBtn2(item)} id="btn-2" className="bg-blue-600 mr-10 ml-2 text-white hover:bg-blue-700 w-60 py-2 rounded-md">Show more</button>
                            </td>
                        </td>
                    </tr>
                ))} 
            </tbody>
        </table>
      </div>
    )
  }
}

export default Table